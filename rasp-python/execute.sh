#!/bin/sh

HOME=/home/pi/DHT
#JAVA_ARGS="-Xms256m -Xmx512m"
JAVA_ARGS=""
#CLASSPATH="${HOME}/properties"
CLASSPATH="."
for x in `ls ${HOME}/lib/*.jar`; do CLASSPATH=$CLASSPATH:$x; done

#PATH=${PATH}:/opt/oracle/jrockit-jdk1.6.0_31-R28.2.3-4.1.0/bin
java ${JAVA_ARGS} -cp ${CLASSPATH} MainClass