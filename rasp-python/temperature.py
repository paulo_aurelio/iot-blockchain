#Tutorial : http://osoyoo.com/?p=858
import dht11
import RPi.GPIO as GPIO
import time

# Define GPIO to Sensor mapping
Temp_sensor=14

def main():
  # Main program block
  GPIO.setwarnings(False)
  GPIO.setmode(GPIO.BCM)       # Use BCM GPIO numbers

  #while True:
  #get DHT11 sensor value
  instance = dht11.DHT11(pin = Temp_sensor)
  result = instance.read()

  if result.is_valid():
	#print "Humidity:"+ str(result.humidity) +"% - Temperature:"+ str(result.temperature) +"C"
	print str(result.humidity) +"ABC"+ str(result.temperature)


if __name__ == '__main__':

  try:
    main()
  except KeyboardInterrupt:
    pass
  finally:
    GPIO.cleanup()
