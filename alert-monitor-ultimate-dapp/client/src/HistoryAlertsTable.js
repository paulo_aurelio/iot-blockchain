import React, { Component } from 'react';
//
// reactstrap components
import {
  Card,
  CardBody,
  CardTitle,
  Table
} from "reactstrap";
//
// Simple Components (Fuctions)
const TableHeader = () => { 
  return (
      <thead>
      <tr>
        <th className="">Horário</th>
        <th className="">Nome da Farmácia</th>
        <th className="">Nome do Sensor</th>
        <th className="text-center ">Temperatura Limite</th>
        <th className="text-center ">Temperatura Medida</th>                
      </tr>
    </thead>
  );
};
//
const TableBody = props => { 

  const rows = props.alertData
      .sort((a, b) => b.timeStamp - a.timeStamp)
      .map((row, index) => {
          return (
              <tr key={index}>  
                <td><span className="text-warning">{row.dateToShow}</span></td>
                <td><span className="text-warning">{row.drogStoreName}</span></td>
                <td><span className="text-warning">{row.sensorName}</span></td>
                <td className="text-center"><span className="text-danger">{row.temperatureThreshold}</span></td>
                <td className="text-center"><span className="text-warning">{row.collectedTemperature}</span></td>
              </tr>
      );
  });
  //
  return <tbody>{rows}</tbody>;
};
//
// Class Components
class HistoryAlertsTable extends Component {
  render() {
    //
    const { historyData } = this.props;
    //
    return (
    <Card>
      <CardBody>
        <CardTitle tag="h4"><span className="">Histórico de Alertas</span></CardTitle>
        <Table className="header-fixed" >
            <TableHeader/>
            <TableBody
               alertData={historyData} 
            />
        </Table>
      </CardBody>
    </Card>
    );
  }
}
//
export default HistoryAlertsTable;
