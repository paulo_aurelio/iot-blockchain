import React, { Component } from 'react';

// reactstrap components
import {
  Card,
  CardBody,
  CardTitle,
  Table
} from "reactstrap";


//
const TableBody = props => { 
  const rows = props.alertData
      .sort((a, b) => b.timeStamp - a.timeStamp)
      .map((row, index) => {
          var dateToShow = new Date().toLocaleTimeString();
          row.dateToShow = dateToShow;         
          return (
            <tr  key={index}>  
              <td><span className="text-warning">{row.dateToShow}</span></td>
              <td><span className="text-warning">{row.drogStoreName}</span></td>
              <td><span className="text-warning">{row.sensorName}</span></td>
              <td><span className="text-danger">{row.temperatureThreshold}</span></td>
              <td><span className="text-warning">{row.collectedTemperature}</span></td>
            </tr>
          );
  });
  //
  return <tbody>{rows}</tbody>;
};
//
class RecentAlertsTable extends Component {
  render() {
    //
    const { recentData } = this.props;
    //
    return (
      <Card>
        <CardBody>
          <CardTitle tag="h4"><span className="">Última Violação Detectada</span></CardTitle>                  
            <Table>               
            <TableBody
               alertData={recentData} 
            />
            </Table>
          </CardBody>
      </Card>
    );
  }
}




export default RecentAlertsTable;
