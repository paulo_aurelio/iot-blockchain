import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Button, 
  Alert
} from "reactstrap";
//
//Component Classes
import RecentAlertsTable from'./RecentAlertsTable';
import HistoryAlertsTable from'./HistoryAlertsTable';
import Clock from './Clock';
//
//
import SensorStateContract from "./contracts/SensorStateContract.json";
//
import getWeb3 from "./utils/getWeb3";
//
//
const onTimerChange = () => {
   return null;
}
//
//onClick Event Handle 
const onClickButtonCreateAlert = (props) => {    
  //Criacao de Eventos  (retirar) 
  var now = new Date();
  var currentTimeStamp = now.getTime().toString();
  props.sensorContract.methods.registerTemperatureEvent(
    currentTimeStamp,
    '40',
    '55',
    'Sensor XYZ').
  send({ 
    from: props.accounts[0],
    gasPrice:'20000000000',
    gas: 1000000
  }).then( receipt  => {
    console.log(receipt);
  });
}
//
const DROG_STORE_NAME ="Drogaria ABC";
//
class App extends Component {
  
  state = { 
    web3: null, 
    accounts: null, 
    sensorContract: null,
    sensorContractAddress: null, 
    eventsCount: null,
    recent_temperature_alerts: [],
    history_temperature_alerts: []
  };
  //
  // 
  componentDidMount = async () => {
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = SensorStateContract.networks[networkId];

      const instanceSensorContract = new web3.eth.Contract(SensorStateContract.abi, deployedNetwork.address);
      //
      // Load Past Events from Blockchain
      instanceSensorContract.getPastEvents('temperatureEvent',
      {
          fromBlock: 0,
          toBlock: 'latest'
      },(error, events) => {
            if (error) {
               console.log('Error in myEvent event handler: ' + error);
            } else {
               console.log('myEvent: ' + JSON.stringify(events));
          
               const historyTemperatureAlerts = events.map( tempAlert => {
                     return ({
                        timeStamp: tempAlert.returnValues.date,
                        drogStoreName: DROG_STORE_NAME,
                        sensorName: tempAlert.returnValues.sensorId,
                        temperatureThreshold: tempAlert.returnValues.maximumAllowedReading,
                        collectedTemperature: tempAlert.returnValues.actualReading
                      });
                 });
                 //
                 //
                 this.setState({
                      web3, 
                      accounts, 
                      sensorContract: instanceSensorContract,
                      sensorContractAddress: deployedNetwork.address,
                      eventsCount: events.length, 
                      history_temperature_alerts: historyTemperatureAlerts
                  }, 
                  this.getCurrentTemperatureAlerts);
        }
      });
      //
      //
      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      //this.setState({ web3, accounts, sensorContract: instanceSensorContract});
      // 
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };
  //
  //  
  getCurrentTemperatureAlerts = () => {

      const {sensorContract, recent_temperature_alerts, history_temperature_alerts} = this.state;
      //
      sensorContract.events.temperatureEvent({
        fromBlock: 'latest'
      }, 
      (error, events)  => { 
          if (error) {
            console.log('Error in myEvent event handler: ' + error);
          } else {

              console.log('myEvent: ' + JSON.stringify(events));
              
              // returns only one event: the current
              const recentTemperatureAlert = tempAlert => {
                  return ({
                      timeStamp: tempAlert.date,
                      drogStoreName: DROG_STORE_NAME,
                      sensorName: tempAlert.sensorId,
                      temperatureThreshold: tempAlert.maximumAllowedReading,
                      collectedTemperature: tempAlert.actualReading
                  });
              };
              //
              this.setState({
                  recent_temperature_alerts: [...this.state.recent_temperature_alerts, recentTemperatureAlert(events.returnValues)]
              });        
          }
            //
      });
  }
  // 
  //
  //
  render() {

    if (!this.state.web3) {
      return (
        <Container>
        <br/>
        <br/>
        <br/>        
        <Row>
          <Col md="12">
            <Alert color="info"><strong>Loading Web3, accounts, and contract...</strong></Alert>
          </Col>
        </Row>
        
        </Container>
      ) 
    }

    if (this.state.recent_temperature_alerts.length > 1){
              
      var oldestEvent = this.state.recent_temperature_alerts.reduce((olderEvent, nextEvent) => {
          return (olderEvent.timeStamp) < nextEvent.timeStamp ? olderEvent : nextEvent;
      }, {});

      this.setState({
           recent_temperature_alerts:   this.state.recent_temperature_alerts.filter(
              (e) => e.timeStamp !== oldestEvent.timeStamp 
           ),
           history_temperature_alerts: [oldestEvent, ...this.state.history_temperature_alerts],
           eventsCount: (this.state.eventsCount + 1)
      });   
     } 

    return (
      <Container>
        <br/>        
        <Row>
          <Col md="12"><h3><span className="text-info">Alertas de Violação na Temperatura de Medicamentos</span></h3></Col>
        </Row>
        <Row>  
          <Col md="6">
              <Clock/>
          </Col>
          <Col md="6">
             <p className="text-right text-success">Endereço no Blockchain: {this.state.sensorContractAddress}</p>
          </Col>
        </Row>
        <Row> 
          <Col md="12">
              <RecentAlertsTable  
                  recentData={this.state.recent_temperature_alerts}              
              />
          </Col>   
          <Col md="12">
          <p className="text-success">Total de Alertas : {this.state.eventsCount}</p>
          </Col>      
          <Col md="12">            
              <HistoryAlertsTable  
                  historyData={this.state.history_temperature_alerts} 
              />                
          </Col>
          </Row>
          <Row>
          <Col className="text-left" md="12">
              <Button onClick={() => onClickButtonCreateAlert(this.state)}><span className="text-info">Enviar Alerta ao Blockchain</span></Button>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;
