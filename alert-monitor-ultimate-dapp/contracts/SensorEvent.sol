pragma solidity ^0.5.0;

contract SensorStateContract {
    
    struct TemperatureAlertEvent {
      string date;
      string maximumAllowedReading;
      string actualReading;
      string sensorId; 
     }
     
     mapping (string => TemperatureAlertEvent) private temperature_events;
     
     // id dos sensores
     string[] sensors;
     
     function getCountofSensors() public view returns (uint) {
         return sensors.length;
     }
     
     
     address owner;
 
     constructor () public {
          owner = msg.sender;
     }
     
     function kill() public { 
       require(msg.sender == owner);     
       selfdestruct (msg.sender);
    }
     
     
     event temperatureEvent( string date,
      string maximumAllowedReading,
      string actualReading,
      string sensorId);
      
     function registerTemperatureEvent (string memory date,
      string memory maximumAllowedReading,
      string memory actualReading,
      string memory sensorId) public {
          
          // adiciona na lista de eventos
          temperature_events[sensorId] = TemperatureAlertEvent(date,
                        maximumAllowedReading,
                        actualReading,
                        sensorId);
        
         // cria um evento
          emit temperatureEvent(date,
                        maximumAllowedReading,
                        actualReading,
                        sensorId);
      }
 
    function getTemperatureEvent(string memory sensorId) 
       view public returns (string memory, string memory, string memory, string memory) {
           
           return(temperature_events[sensorId].date,
           temperature_events[sensorId].maximumAllowedReading,
           temperature_events[sensorId].actualReading,
           temperature_events[sensorId].sensorId);
           
       }
 }