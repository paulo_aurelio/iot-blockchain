const Web3 = require('web3');
const express = require('express');
var bodyParser     =        require("body-parser");

const ABI = ` [
    {
      "inputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor",
      "signature": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "date",
          "type": "string"
        },
        {
          "indexed": false,
          "name": "maximumAllowedReading",
          "type": "string"
        },
        {
          "indexed": false,
          "name": "actualReading",
          "type": "string"
        },
        {
          "indexed": false,
          "name": "sensorId",
          "type": "string"
        }
      ],
      "name": "temperatureEvent",
      "type": "event",
      "signature": "0x3ada298eaea59fec5d119b89b890b188a15f5d88de17129185a3899254b1e77b"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "getCountofSensors",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function",
      "signature": "0x9013bd9d"
    },
    {
      "constant": false,
      "inputs": [],
      "name": "kill",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0x41c0e1b5"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "date",
          "type": "string"
        },
        {
          "name": "maximumAllowedReading",
          "type": "string"
        },
        {
          "name": "actualReading",
          "type": "string"
        },
        {
          "name": "sensorId",
          "type": "string"
        }
      ],
      "name": "registerTemperatureEvent",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0x8a1b7efe"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "sensorId",
          "type": "string"
        }
      ],
      "name": "getTemperatureEvent",
      "outputs": [
        {
          "name": "",
          "type": "string"
        },
        {
          "name": "",
          "type": "string"
        },
        {
          "name": "",
          "type": "string"
        },
        {
          "name": "",
          "type": "string"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function",
      "signature": "0xbb4e05d5"
    }
  ]`;


const app = express();
const router = express.Router();
const port = 8080;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// url: http://localhost:3000/
app.get('/', (request, response) => response.send('Hello World'));

// all routes prefixed with /api
app.use('/events', router);

// using router.get() to prefix our path
// url: http://localhost:3000/api/
router.get('/', (request, response) => {
  response.json({message: 'Hello, welcome to my server'});
});

router.post('/', (request, response) => {
    //response.json({message: 'Hello, welcome to my server'});
    //console.log(request);
    console.log(String(request.body.sensorId));
    console.log(String(request.body.maximumAllowedReading));
    console.log(String(request.body.actualReading));
	console.log(String(request.body.timestamp));
	
	var provider = new Web3.providers.WebsocketProvider('ws://127.0.0.1:8545');
    var web3 = new Web3();
    web3.setProvider(provider);
    //var web3 = new Web3("ws://192.168.56.101:8486");

	//web3.eth.defaultAccount = web3.eth.accounts[0];
	
	//const accounts = web3.eth.getAccounts();
  // 
    //web3.eth.personal.unlockAccount(COINBASE_ADDRESS, "arquitetura", 600).then(console.log('Account unlocked!'));

  var contractInstance = new web3.eth.Contract(JSON.parse(ABI),"0x5619743E425671CB1E37b81d4e5afE1366F063C0");
                                                               
  // contractInstance.options.address = ;
  var COINBASE_ADDRESS = "0x9dfcf0e2b63f24121b5b7b2ceff941683308fc97";

  contractInstance.methods.registerTemperatureEvent(
	String(request.body.timestamp),
	String(request.body.maximumAllowedReading),
	String(request.body.actualReading),
	String(request.body.sensorId)).send({ 
			from: COINBASE_ADDRESS,
			gasPrice:'20000000000',
			gas: 1000000
}).then( receipt  => {
	console.log('Transacao Inserida no Blockchain');
	console.log(receipt.transactionHash);
	
   var resp = {
				transactionHash : receipt.transactionHash,
				transactionIndex : receipt.transactionIndex,
				blockHash: receipt.blockHash,
				blockNumber: receipt.blockNumber
				};
			response.send(JSON.stringify(resp));
			//response.send("leu");
});
  
});

/**
 * 
 * 
 */
		
	

// set the server to listen on port 3000
app.listen(port, () => console.log(`Listening on port ${port}`));