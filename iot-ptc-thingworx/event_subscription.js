logger.error("Erro - Temperatura fora do padr�o: " + eventData.value + ", timestamp: "+ eventTime);

var payload = {
 "sensorId":"Sensor2",
 "maximumAllowedReading":me.TemperatureLimit,
 "actualReading":eventData.value,
 "timestamp":eventTime,
 "type":"0" 
};

var params = {
	proxyScheme: undefined /* STRING */,
	headers: {"Content-Type": "application/json","Accept": "application/json"},
	ignoreSSLErrors: true,
	url: me.urlConfig,
	content: payload,
	timeout: 50000 /* NUMBER */
};


if(me.postToBlockchain) {
    
    if("temperature" === eventData.description) {
        // result: JSON
        logger.error("Violacao de temperatura sera registrada no Blockchain: ");
        var result = Resources["ContentLoaderFunctions"].PostJSON(params);
        //logger.error("DEBUG: objeto retornado: " + JSON.stringify(result));
        var newRow = new Object();
        newRow.blockHash = result.blockHash;
        newRow.blockNumber = result.blockNumber;
        newRow.transactionIndex = result.transactionIndex;
        newRow.transactionHash = result.transactionHash;
        me.LastBCEvent.AddRow(newRow);
        // logger.error("DEBUG: INFOTABLE Construida" + me.LastBCEvent.ToJSON());
    }
 }