

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thingworx.communications.client.ConnectedThingClient;
import com.thingworx.communications.client.things.VirtualThing;
import com.thingworx.metadata.FieldDefinition;
import com.thingworx.metadata.annotations.ThingworxEventDefinition;
import com.thingworx.metadata.annotations.ThingworxEventDefinitions;
import com.thingworx.metadata.annotations.ThingworxPropertyDefinition;
import com.thingworx.metadata.annotations.ThingworxPropertyDefinitions;
import com.thingworx.metadata.annotations.ThingworxServiceDefinition;
import com.thingworx.metadata.collections.FieldDefinitionCollection;
import com.thingworx.types.BaseTypes;
import com.thingworx.types.collections.ValueCollection;
import com.thingworx.types.constants.CommonPropertyNames;
import com.thingworx.types.primitives.NumberPrimitive;
import com.thingworx.types.primitives.StringPrimitive;

// Refer to the "Steam Sensor Example" section of the documentation
// for a detailed explanation of this example's operation

// Property Definitions
@SuppressWarnings("serial")
@ThingworxPropertyDefinitions(properties = {
		@ThingworxPropertyDefinition(name="Temperature", description="Current Temperature", baseType="NUMBER", category="Status", aspects={"isReadOnly:true"}),
		@ThingworxPropertyDefinition(name="Humidity", description="Current Humidity", baseType="NUMBER", category="Status", aspects={"isReadOnly:true"}),
		@ThingworxPropertyDefinition(name="Pressure", description="Current Pressure", baseType="NUMBER", category="Status", aspects={"isReadOnly:true"}),
		@ThingworxPropertyDefinition(name="FaultStatus", description="Fault status", baseType="BOOLEAN", category="Faults", aspects={"isReadOnly:true"}),
		@ThingworxPropertyDefinition(name="InletValve", description="Inlet valve state", baseType="BOOLEAN", category="Status", aspects={"isReadOnly:true"}),
		@ThingworxPropertyDefinition(name="TemperatureLimit", description="Temperature fault limit", baseType="NUMBER", category="Faults", aspects={"isReadOnly:false"}),
		@ThingworxPropertyDefinition(name="TotalFlow", description="Total flow", baseType="NUMBER", category="Aggregates", aspects={"isReadOnly:true"}),
		@ThingworxPropertyDefinition(name="HumidityLimit", description="Humidity fault limit", baseType="NUMBER", category="Faults", aspects={"isReadOnly:false"})
	})

	@ThingworxEventDefinitions(events = {
		@ThingworxEventDefinition(name="SteamSensorFault", description="Steam sensor fault", dataShape="SteamSensor.Fault", category="Faults", isInvocable=true, isPropertyEvent=false)
	})

public class RaspberriThing extends VirtualThing  {
	
	private static final Logger LOG = LoggerFactory.getLogger(MainClass.class);


    public RaspberriThing(String name, String description, ConnectedThingClient client) throws Exception {
        super(name, description, client);


		// Define the Data Shape used by the SteamSensor.Fault event.
		// mesmo Data Shape deve ser criado manualmente no TW Platform
		FieldDefinitionCollection faultFields = new FieldDefinitionCollection();
		faultFields.addFieldDefinition(new FieldDefinition(CommonPropertyNames.PROP_MESSAGE, BaseTypes.STRING));
		faultFields.addFieldDefinition(new FieldDefinition(CommonPropertyNames.PROP_VALUE, BaseTypes.NUMBER));
        faultFields.addFieldDefinition(new FieldDefinition(CommonPropertyNames.PROP_DESCRIPTION, BaseTypes.STRING));
		defineDataShapeDefinition("SteamSensor.Fault", faultFields);

		// Copy all the default values from the aspects of the properties defined above to this
		// instance. This gives it an initial state but does not push it to the server.
		initializeFromAnnotations();
    }

    // From the VirtualThing class
    // This method will get called when a connect or reconnect happens
    // Need to send the values when this happens
    // This is more important for a solution that does not send its properties on a regular basis
   
    /**public void synchronizeState() {
        // Be sure to call the base class
        super.synchronizeState();
        // Send the property values to Thingworx when a synchronization is required
        super.syncProperties();
    }
   */
    
    // The processScanRequest is called by the SteamSensorClient every scan cycle
    @Override
    public void processScanRequest() throws Exception {
        // Be sure to call the base classes scan request
        super.processScanRequest();
        // Execute the code for this simulation every scan
        this.scanDevice();
    }

    // Performs the logic for the steam sensor, occurs every scan cycle
    public void scanDevice() throws Exception {
    	
    			double temperature = 0.0;
    			double humidity = 0.0;
    			
    			if (Configurations.DEBUG_MODE) {
    				
    				LOG.info("================ DEBUG MODE - random generated values");
    				
    				// FAKE DATA POC - temperatura - 10 a 90 - se passar de TemperatureLimit (enviado pela plataforma), gera evento de alerta
        			temperature = 10 + 81 * Math.random();
        			humidity = 0 + 101 * Math.random();
    				
    			} else {
    				/*
        			 * leitura do Python aqui
        			 */
    				try {
    					LOG.info("================ REAL THING - reading from sensor");
	    				String line ="0.0";
	    				String[] data;
	    				Runtime rt = Runtime.getRuntime();
	    				Process p = rt.exec("python /home/pi/DHT/temperature.py");
	    				 BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
	    				 while((line = bri.readLine()) != null) {
	    					 if(!(line.contains("ERR_CRC") || line.contains("ERR_RNG"))){	
	    						 LOG.debug("================ Retorno Python Script "+line);
	    						 data=line.split("ABC");
	    						 humidity=Double.parseDouble(data[0].trim());
	    						 temperature=Double.parseDouble(data[1].trim());
	    						 //System.out.println(line);
	    					 } else { 
	    						// System.out.println("Data Error");
	    						 LOG.error("================ Erro ao coletar dados dos sensores - Python Script: ERR_CRC ou ERR_RNG");
	    					 }
	    				 }
	    				 bri.close();
	    				 p.waitFor();
    				} catch (Exception ex) {
    					LOG.error("================ Erro ao coletar dados dos sensores - Python Script "+ex.getMessage());
    				}
    				
    			}
    			
    			// POC - propriedades lidas pelos sensores
    			setProperty("Humidity", humidity);
    			setProperty("Temperature", temperature);

    			// POC - config retornados pelos servidores
    			double temperatureLimit = (Double)getProperty("TemperatureLimit").getValue().getValue();
    			double humidityLimit = (Double)getProperty("HumidityLimit").getValue().getValue();
    			LOG.info("================ Valores de config recebidos da plataforma: Temepratura Limite: "+temperatureLimit + ";  Umidade Limite: "+humidityLimit);

    	        boolean faultStatus = false;

    	        // POC - evento de erro - temperatura excedida
    	        if(temperatureLimit > 0 && temperature > temperatureLimit) {
    	            faultStatus = true;
    	            ValueCollection eventInfo = new ValueCollection();
    	            eventInfo.put(CommonPropertyNames.PROP_MESSAGE,
    	                    new StringPrimitive("Temperatura detectada " + temperature + " esta acima do limite configurado: " + temperatureLimit));
    	            eventInfo.put(CommonPropertyNames.PROP_VALUE, new NumberPrimitive(temperature));
    	            eventInfo.put(CommonPropertyNames.PROP_DESCRIPTION, new StringPrimitive("temperature"));
    	            queueEvent("SteamSensorFault", DateTime.now(), eventInfo);
    	        }

    	        // POC - evento de erro - humidade excedida
    	        /**if(humidityLimit > 0 && humidity > humidityLimit) {
    	            faultStatus = true;
    	            ValueCollection eventInfo = new ValueCollection();
    	            eventInfo.put(CommonPropertyNames.PROP_MESSAGE,
    	                    new StringPrimitive("Humidade detectada " + humidity + " esta acima do limite configurado: " + humidityLimit));
    	            eventInfo.put(CommonPropertyNames.PROP_VALUE, new NumberPrimitive(humidity));
    	            eventInfo.put(CommonPropertyNames.PROP_DESCRIPTION, new StringPrimitive("humidity"));
    	            queueEvent("SteamSensorFault", DateTime.now(), eventInfo);
    	        }*/


    			setProperty("FaultStatus", faultStatus);

    			updateSubscribedProperties(15000);
    			updateSubscribedEvents(60000);
    }
    
    @ThingworxServiceDefinition( name="Shutdown", description="Shutdown the client")
	public void Shutdown() throws Exception {
		this.getClient().shutdown();
	}


}
